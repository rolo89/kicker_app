import React from 'react';

import 'bootstrap/dist/css/bootstrap.min.css'
import './App.css';
import AppNavbar from './components/AppNavbar';
import TeamList from './components/TeamList';
import TeamModal from './components/TeamModal';
import { Container } from 'reactstrap';

import { Provider } from 'react-redux';
import store from './store';

function App()
{
  return (
    <Provider store={store}>
      <div className="App">
        <AppNavbar />
        <Container>
          <TeamModal />
          <TeamList />
        </Container>
      </div>
    </Provider>
  );
}

export default App;

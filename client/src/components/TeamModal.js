import React, { Component } from 'react';
import
{
    Button,
    Modal,
    ModalHeader,
    ModalBody,
    Form,
    FormGroup,
    Label,
    Input
} from 'reactstrap';
import { connect } from 'react-redux';
import { addTeam } from '../actions/teamActions';

class TeamModal extends Component
{
    state = {
        modal: false,
        name: ''
    }

    toggle = () =>
    {
        this.setState({
            modal: !this.state.modal
        });
    };

    onSubmit = e =>
    {
        e.preventDefault();
        const newTeam = {
            name: this.state.name
        };

        //add item via addItem action
        this.props.addTeam(newTeam);

        //close modal
        this.toggle();
    }

    onChange = (e) =>
    {
        this.setState({ [e.target.name]: e.target.value });
    }

    render()
    {
        return (
            <div>
                <Button
                    color="dark"
                    style={{ marginBottom: '2rem' }}
                    onClick={this.toggle}
                >Add Team
                </Button>
                <Modal isOpen={this.state.modal} toggle={this.toggle}>
                    <ModalHeader toggle={this.toggle}>
                        Add To Team
                    </ModalHeader>
                    <ModalBody>
                        <Form onSubmit={this.onSubmit}>
                            <FormGroup>
                                <Label for="team_name">
                                    Team Name
                                </Label>
                                <Input type="text" name="name" id="team_name" placeholder="Add Team Name" onChange={this.onChange} />
                                <Button color="dark" style={{ marginTop: '2rem' }} block>
                                    Add Team
                                </Button>
                            </FormGroup>
                        </Form>
                    </ModalBody>
                </Modal>
            </div>
        );
    }


}

const mapStateToProps = (state) => ({
    item: state.item
});

export default connect(mapStateToProps, { addTeam })(TeamModal);
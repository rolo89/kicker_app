import React, { Component } from 'react';
import { Container, ListGroup, ListGroupItem, Button } from 'reactstrap';
import { CSSTransition, TransitionGroup } from 'react-transition-group';
import { connect } from 'react-redux';
import { getTeams, deleteTeam, addTeam } from '../actions/teamActions';
import PropTypes from 'prop-types';

class TeamList extends Component
{

    componentDidMount()
    {
        this.props.getTeams();
    };

    onDeleteTeamClick = id =>
    {
        this.props.deleteTeam(id);
    }

    render()
    {
        const { teams } = this.props.item;
        return (
            <Container>
                <ListGroup>
                    <TransitionGroup className="team-list">
                        {teams.map(({ _id, name }) => (
                            <CSSTransition key={_id} timeout={500} classNames="fade">
                                <ListGroupItem>
                                    <Button
                                        className="remove-btn"
                                        color="danger"
                                        size="sm"
                                        onClick={this.onDeleteTeamClick.bind(this, _id)}
                                    >&times;</Button>
                                    {name}
                                </ListGroupItem>
                            </CSSTransition>
                        ))}
                    </TransitionGroup>
                </ListGroup>
            </Container>
        );
    }
}
TeamList.propTypes = {
    getTeams: PropTypes.func.isRequired,
    item: PropTypes.object.isRequired
};

const mapStateToProps = (state) => ({
    item: state.item
});

export default connect(mapStateToProps, { getTeams, addTeam, deleteTeam })(TeamList);
const express = require('express');
const mongoose = require('mongoose');
const bodyParser = require('body-parser');
const teams = require('./routes/api/teams');
const player = require('./routes/api/player');
const path = require('path');

const app = express();

//bodyparser middleware
app.use(bodyParser.json());

//DB config
const db = require('./config/keys').mongoURI;

//connect to mongo
mongoose.connect(db, {
    useNewUrlParser: true,
    useUnifiedTopology: true
})
    .then(() => console.log('Mongo DB connected'))
    .catch(err => console.log(err));

//set routes
app.use('/api/teams', teams);
app.use('/api/player', player);

//check if running on prod server like heroku or aws and setting up environment appropriately
if (process.env.NODE_ENV === 'production')
{
    //set static folder
    app.use(express.static('client/build'));
    //direct every request (except api requests from above) to .../client/build/index.html
    app.get('*', (req, res) =>
    {
        res.sendFile(path.resolve(__dirname, 'client', 'build', 'index.html'));
    });
}

//process.env.PORT is taken from prod server
const port = process.env.PORT || 5000;

app.listen(port, () => console.log(`Server started on port ${port}`));
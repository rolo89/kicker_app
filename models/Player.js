const mongoose = require('mongoose');
const Schema = mongoose.Schema;

//create Schema
const PlayerSchema = new Schema({
    team: { type: Schema.Types.ObjectId, ref: 'Team' },
    name: {
        type: String,
        required: true,
        minlength: [1, 'Name is too short']
    }
});

module.exports = mongoose.model('Player', PlayerSchema);
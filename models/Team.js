const mongoose = require('mongoose');
const Schema = mongoose.Schema;

//create Schema
const TeamSchema = new Schema({
    name: {
        type: String,
        required: true,
        minlength: [1, 'Team name is too short']
    },
});

module.exports = mongoose.model('Team', TeamSchema);
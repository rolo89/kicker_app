const express = require('express');
const router = express.Router();

//Team Model
const Team = require('../../models/Team');

/**
 * @route GET api/teams
 * @desc Get All Teams
 * @access Public
 */
router.get('/', (req, res) =>
{
    Team.find()
        .sort({
            name: 1
        })
        .then(teams => res.json(teams));
});

/**
 * @route POST api/teams
 * @desc Add new Team
 * @access Public
 */
router.post('/', (req, res) =>
{
    const newTeam = new Team(req.body);
    newTeam.save().then(team => res.json(team)).catch(err => res.status(404).json(err));
});


/**
 * @route DELETE api/teams/:id
 * @desc Delete Team by ID
 * @access Public
 */
router.delete('/:id', (req, res) =>
{
    Team.findById(req.params.id).then(team => team.remove().then(() => res.json({
        success: true
    }))).catch(err => res.status(404).json({
        success: false
    }));

});



module.exports = router;
const express = require('express');
const router = express.Router();

//Player Model
const Player = require('../../models/Player');
const Team = require('../../models/Team');

/**
 * @route GET api/player
 * @desc Get All Player
 * @access Public
 */
router.get('/', (req, res) =>
{
    Player.find()
        .sort({
            name: 1
        }).
        populate('team')
        .then(player => res.json(player));
});

/**
 * @route POST api/player
 * @desc Add new Player
 * @access Public
 */
router.post('/', (req, res) =>
{
    //exclude team
    const { team: foo, ...playerWithoutTeam } = req.body;
    const newPlayer = new Player(playerWithoutTeam);
    newPlayer.save().then(player => res.json(player)).catch(err => res.status(404).json(err));
});


/**
 * @route DELETE api/player/:id
 * @desc Delete Player by ID
 * @access Public
 */
router.delete('/:id', (req, res) =>
{
    Player.findById(req.params.id).then(player => player.remove().then(() => res.json({
        success: true
    }))).catch(err => res.status(404).json({
        success: false
    }));

});


/**
 * @route PUT api/player/:id/setTeam/:teamId
 * @desc Set Team
 * @access Public
 */
router.put('/:id/setTeam/:teamId', (req, res) =>
{
    //check if team exists
    Team.findById(req.params.teamId).count().
        then(count =>
        {
            //given team does exist
            if (count > 0)
            {
                Player.findByIdAndUpdate(req.params.id, { $set: { team: req.params.teamId } }).
                    then((player) => res.json(player)).
                    catch(err => res.status(404).json(err));
            }
            //given team does not exist
            else
            {
                res.status(404).json("Team does not exist")
            }

        })
        .catch(err => res.status(404).json(err));
});








module.exports = router;